package in.userlogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.userlogin.Model.Users;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class Login extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private TextView result;
    private Button login;

    private Realm myRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        result = (TextView) findViewById(R.id.result);
        login = (Button) findViewById(R.id.login_account);


        myRealm = Realm.getInstance(new RealmConfiguration.Builder(this)
                .name("users.realm")
                .build());

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateTextField()) {
                    loginUser();
                }
            }
        });
    }

    private void loginUser() {
        RealmResults<Users> results1 =
                myRealm.where(Users.class).findAll();

        int userCount = 0;
        for (Users c : results1) {
            Log.d("results", c.getEmail());

            if (email.getText().toString().equals(c.getEmail())) {
                result.setText(c.getName() + "\n" +
                        c.getEmail());

                Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_LONG).show();
                break;
            }
        }
    }


    private Boolean validateTextField() {
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();

        email.setError(null);
        password.setError(null);
        Boolean isValid = true;

        if (TextUtils.isEmpty(userEmail)) {
            email.setError("enter your email");
            email.requestFocus();
            isValid = false;
        } else if (TextUtils.isEmpty(userPassword)) {
            password.setError("enter password");
            password.requestFocus();
            isValid = false;
        }


        return isValid;
    }
}
