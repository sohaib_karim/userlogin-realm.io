package in.userlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.userlogin.Model.Users;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SignUp extends AppCompatActivity {

    private EditText name;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private Button createAccount;

    private Realm myRealm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        createAccount = (Button) findViewById(R.id.create_account);

        myRealm = Realm.getInstance(new RealmConfiguration.Builder(this)
                .name("users.realm")
                .build());

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateTextField()) {

                    addUserToDB();
                }
            }
        });
    }

    private void addUserToDB() {
        try {
            myRealm.beginTransaction();

            Users users = myRealm.createObject(Users.class);
            users.setName(name.getText().toString());
            users.setEmail(email.getText().toString());
            users.setPassword(password.getText().toString());

            myRealm.commitTransaction();

            Intent intent = new Intent(SignUp.this, MainActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Value already exist", Toast.LENGTH_LONG).show();
        }
    }

    private Boolean validateTextField() {
        String userName = name.getText().toString();
        String userEmail = email.getText().toString();
        String userPassword = password.getText().toString();
        String userConfirmPassword = confirmPassword.getText().toString();

        name.setError(null);
        email.setError(null);
        password.setError(null);
        confirmPassword.setError(null);
        Boolean isValid = true;

        if (TextUtils.isEmpty(userName)) {
            name.setError("Enter user name");
            name.requestFocus();
            isValid = false;
        } else if (TextUtils.isEmpty(userEmail)) {
            email.setError("enter your email");
            email.requestFocus();
            isValid = false;
        } else if (TextUtils.isEmpty(userPassword)) {
            password.setError("enter password");
            password.requestFocus();
            isValid = false;
        } else if (TextUtils.isEmpty(userConfirmPassword)) {
            confirmPassword.setError("enter passsword");
            confirmPassword.requestFocus();
            isValid = false;
        }

        if (!TextUtils.isEmpty(userPassword) && !TextUtils.isEmpty(userConfirmPassword)) {
            if (!userPassword.equals(userConfirmPassword)) {
                isValid = false;
                password.setError("password do not match");
                confirmPassword.setError("password do not match");
                confirmPassword.requestFocus();
            }
        }


        return isValid;
    }
}
